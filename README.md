# Cyannotation

Cyannotation color ontology

https://bioportal.bioontology.org/ontologies/CYAN

## Run the project

poetry run python3 build.py prod
poetry run python3 build.py dev

## Contributing

We're open to this, if you're interested! Early days.

## License

MIT
