from rdflib import Literal, ConjunctiveGraph, URIRef, BNode
from rdflib.namespace import DCTERMS, OWL
from namespace_definitions import *
from datetime import date
from config import *

########################
# LOCAL NAMESPACES
########################

########################
# Classes
########################

colorClass = cyanNS["Color"]
colorAttributeClass = cyanNS["ColorAttribute"]
hueClass = cyanNS["Hue"]
saturationClass = cyanNS["Saturation"]
lightnessClass = cyanNS["Lightness"]
metallicSheenClass = cyanNS["MetallicSheen"]
colorCategoryClass = cyanNS["ColorCategory"]
opacityClass = cyanNS["Opacity"]
glossClass = cyanNS["Gloss"]
attributeSetClass = cyanNS["AttributeSet"]
hueSetClass = cyanNS["HueSet"]
saturationSetClass = cyanNS["SaturationSet"]
lightnessSetClass = cyanNS["LightnessSet"]
metallicSheenSetClass = cyanNS["MetallicSheenSet"]
colorCategorySetClass = cyanNS["ColorCategorySet"]
opacitySetClass = cyanNS["OpacitySet"]
glossSetClass = cyanNS["GlossSet"]

########################
# Object Properties
########################

hasLightness = cyanNS["hasLightness"]
lightnessOf = cyanNS["lightnessOf"]

hasSaturation = cyanNS["hasSaturation"]
saturationOf = cyanNS["saturationOf"]

hasHue = cyanNS["hasHue"]
hueOf = cyanNS["hueOf"]

hasMetallicSheen = cyanNS["hasMetallicSheen"]
metallicSheenOf = cyanNS["metallicSheenOf"]

hasColorCategory = cyanNS["hasColorCategory"]
colorCategoryOf = cyanNS["colorCategoryOf"]

hasOpacity = cyanNS["hasOpacity"]
opacityOf = cyanNS["opacityOf"]

hasGloss = cyanNS["hasGloss"]
glossOf = cyanNS["glossOf"]

memberOfSet = cyanNS["memberOfSet"]
hasSetMember = cyanNS["hasSetMember"]

variantOf = cyanNS["variantOf"]
hasVariant = cyanNS["hasVariant"]

subAttributeOf = cyanNS["subAttributeOf"]
hasSubAttribute = cyanNS["hasSubAttribute"]

favouriteColorOf = cyanNS["favouriteColorOf"]
hasFavouriteColor = cyanNS["hasFavouriteColor"]

########################
# Data Properties
########################

typicalHsl = cyanNS["typicalHsl"]
description = cyanNS["description"]
name = cyanNS["name"]
preferredDescriptor = cyanNS["preferredDescriptor"]

########################
# GRAPH 
########################

def create_graph():
    """
    Creates a new graph and adds the ontology namespace
    """
    graph = ConjunctiveGraph()

    # Description of the ontology itself
    metaTriples = [
        (URIRef(NAMESPACE_URI), rdfType, owlOntology),
        (URIRef(NAMESPACE_URI), dcTermsTitle, Literal("Cyannotation")),
        (URIRef(NAMESPACE_URI), dcTermsCreated, Literal("2022-06-04", datatype=xsdDateTime)),
		(URIRef(NAMESPACE_URI), dcTermsModified, Literal(date.today(), datatype=xsdDateTime)),
        (URIRef(NAMESPACE_URI), owlVersionInfo, Literal("0.1 (alpha), do not use yet, but please contribute!")),
        (URIRef(NAMESPACE_URI), vannPreferredNamespacePrefix, Literal(NAMESPACE_PREFIX)),
        (URIRef(NAMESPACE_URI), sdoCodeRepositoryProperty, URIRef(CODE_REPOSITORY_URL)),
        (URIRef(NAMESPACE_URI), dcTermsLicense, URIRef("https://ontology.lswr.to/mit-license.html")),
        (URIRef(NAMESPACE_URI), dcTermsDescription, Literal(DESCRIPTION)),
        (URIRef(NAMESPACE_URI), skosHistoryNote, Literal("This ontology was originally created to support the Dreamcloud dream journal app. However it can be used for other purposes. Please contact the team if you have any questions.")),
        (URIRef(NAMESPACE_URI), rdfsSeeAlso, dbpIsccNbs),
        # Contributors (blank nodes)

        (URIRef(NAMESPACE_URI), dcTermsPublisher, BNode("creator1")),
        (BNode("creator1"), sdoName, Literal("Dreamcloud")),
        (BNode("creator1"), foafHomepage, URIRef("https://dreamcloud.app/")),
        # contributor1
        (URIRef(NAMESPACE_URI), dcTermsCreator, BNode("contributor1")),
        (BNode("contributor1"), sdoName, Literal("Dan Kennedy")),
        (BNode("contributor1"), sdoEmail, Literal("dannykennedy@email.com")),
        (BNode("contributor1"), sdoIdentifier, URIRef("https://orcid.org/0000-0003-3050-3488")),
        (BNode("contributor1"), foafHomepage, URIRef("https://dannykennedy.co/")),
        (BNode("contributor1"), sdoAffiliation, URIRef("https://dreamcloud.app/")),
        # (BNode("contributor1"), hasFavouriteColor, cyanNS["DuskyLightSpringGreen"]),
        # contributor2
        (URIRef(NAMESPACE_URI), dcTermsCreator, BNode("contributor2")),
        (BNode("contributor2"), sdoName, Literal("Gez Quinn")),
        (BNode("contributor2"), sdoIdentifier, URIRef("https://orcid.org/0000-0002-4913-5139")),
        (BNode("contributor2"), foafHomepage, URIRef("https://gezquinn.design/")),
        (BNode("contributor2"), sdoAffiliation, URIRef("https://dreamcloud.app/")),
        # (BNode("contributor2"), hasFavouriteColor, cyanNS["VividDarkMagentaRed"]),
        # (BNode("contributor2"), hasFavouriteColor, cyanNS["VividDarkBlueGreen"]),
        # (BNode("contributor2"), hasFavouriteColor, cyanNS["DarkForestGreen"]),
        # Contributor3
        (URIRef(NAMESPACE_URI), dcTermsCreator, BNode("contributor3")),
        (BNode("contributor3"), sdoName, Literal("Sheldon Juncker")),
        (BNode("contributor3"), foafHomepage, URIRef("https://jundar.dev/")),
        (BNode("contributor3"), sdoAffiliation, URIRef("https://dreamcloud.app/")),
        (BNode("contributor3"), sdoIdentifier, URIRef("https://orcid.org/0000-0002-1398-5027")),
    ]

    ########################
    # CLASS DEFINITIONS
    ########################

    classTriples = [

        # Hue
        (hueClass, rdfType, owlClass),
        (hueClass, rdfsComment, Literal("Hue of a color")),
        (hueClass, rdfsLabel, Literal("Hue")),
        (hueClass, rdfsSeeAlso, dbpediaNS['Hue']),

        # Saturation
        (saturationClass, rdfType, owlClass),
        (saturationClass, rdfsComment, Literal("Saturation of a color")),
        (saturationClass, rdfsLabel, Literal("Saturation")),

        # Lightness
        (lightnessClass, rdfType, owlClass),
        (lightnessClass, rdfsComment, Literal("Lightness of a color")),
        (lightnessClass, rdfsLabel, Literal("Lightness")),

        # Metallic Sheen
        (metallicSheenClass, rdfType, owlClass),
        (metallicSheenClass, rdfsComment, Literal("Metallic Sheen of a color")),
        (metallicSheenClass, rdfsLabel, Literal("Metallic Sheen")),

        # Color Category
        (colorCategoryClass, rdfType, owlClass),
        (colorCategoryClass, rdfsComment, Literal("The 'type' of color that a color is perceived to be. An ideal colour category in a given language or system is a colour that cannot be said to be a 'type' of any other colour. For example in English, 'green' is a color category. Magenta is dubious as a colour category in English, since many people would identify magentas as types of purple or pink. However some color systems may choose to have categories that are not defined strictly in this way. Intermediate colors may be perceived as having multiple 'types' (e.g. turquoise is a type of blue, and a type of green). In English and many other languages, there appears to be around 11 natural color categories. In many languages there are different numbers.")),
        (colorCategoryClass, rdfsLabel, Literal("Color Category")),

        # Opacity
        (opacityClass, rdfType, owlClass),
        (opacityClass, rdfsComment, Literal("Opacity of a color. This is an linguistic approximation of a range of opacity/transparency, not an exact value.")),
        (opacityClass, rdfsLabel, Literal("Opacity")),
        (opacityClass, rdfsSeeAlso, dbpOpacity),

        # Gloss
        (glossClass, rdfType, owlClass),
        (glossClass, rdfsComment, Literal("Gloss of a color")),
        (glossClass, rdfsLabel, Literal("Gloss")),

        # Color
        (colorClass, rdfType, owlClass),
        (colorClass, rdfsComment, Literal("A color is defined by a combination of color attributes, e.g. 'light purple' (lightness of 'light', hue of 'purple'). It need not be a specific color with a specific HSL value.")),
        (colorClass, rdfsLabel, Literal("Color")),
        (colorClass, owlEquivalentClass, dboColorClass),
 
        # Color Attribute
        (colorAttributeClass, rdfType, owlClass),
        (colorAttributeClass, rdfsComment, Literal("An attribute of a color. For example, a color may have a hue, saturation, lightness, or metallic sheen.")),
        (colorAttributeClass, rdfsLabel, Literal("Color Attribute")),

        # Attribute Set
        (attributeSetClass, rdfType, owlClass),
        (attributeSetClass, rdfsComment, Literal("A set of color attributes. For example a set of hues, a set of saturations, a set of lightnesses, or a set of metallic sheens.")),
        (attributeSetClass, rdfsLabel, Literal("Attribute Set")),

        # Hue Set
        (hueSetClass, rdfType, owlClass),
        (hueSetClass, rdfsComment, Literal("A set of hues")),
        (hueSetClass, rdfsLabel, Literal("Hue Set")),

        # Saturation Set
        (saturationSetClass, rdfType, owlClass),
        (saturationSetClass, rdfsComment, Literal("A set of saturations")),
        (saturationSetClass, rdfsLabel, Literal("Saturation Set")),

        # Lightness Set
        (lightnessSetClass, rdfType, owlClass),
        (lightnessSetClass, rdfsComment, Literal("A set of lightnesses")),
        (lightnessSetClass, rdfsLabel, Literal("Lightness Set")),

        # Metallic Sheen Set
        (metallicSheenSetClass, rdfType, owlClass),
        (metallicSheenSetClass, rdfsComment, Literal("A set of metallic sheens")),
        (metallicSheenSetClass, rdfsLabel, Literal("Metallic Sheen Set")),

        # Color Category Set
        (colorCategorySetClass, rdfType, owlClass),
        (colorCategorySetClass, rdfsComment, Literal("A set of color categories")),
        (colorCategorySetClass, rdfsLabel, Literal("Color Category Set")),

        # Opacity Set
        (opacitySetClass, rdfType, owlClass),
        (opacitySetClass, rdfsComment, Literal("A set of opacities")),
        (opacitySetClass, rdfsLabel, Literal("Opacity Set")),

        # Gloss Set
        (glossSetClass, rdfType, owlClass),
        (glossSetClass, rdfsComment, Literal("A set of gloss values")),
        (glossSetClass, rdfsLabel, Literal("Gloss Set")),
    ]

    ########################
    # SUBCLASSES
    ########################

    subclassTriples = [
        (hueClass, rdfsSubClassOf, colorAttributeClass),
        (saturationClass, rdfsSubClassOf, colorAttributeClass),
        (lightnessClass, rdfsSubClassOf, colorAttributeClass),
        (metallicSheenClass, rdfsSubClassOf, colorAttributeClass),
        (colorCategoryClass, rdfsSubClassOf, colorAttributeClass),
        (opacityClass, rdfsSubClassOf, colorAttributeClass),
        (glossClass, rdfsSubClassOf, colorAttributeClass),
        (hueSetClass, rdfsSubClassOf, attributeSetClass),
        (saturationSetClass, rdfsSubClassOf, attributeSetClass),
        (lightnessSetClass, rdfsSubClassOf, attributeSetClass),
        (metallicSheenSetClass, rdfsSubClassOf, attributeSetClass),
        (colorCategorySetClass, rdfsSubClassOf, attributeSetClass),
        (opacitySetClass, rdfsSubClassOf, attributeSetClass),
        (glossSetClass, rdfsSubClassOf, attributeSetClass),
    ]

    ########################
    # OBJECT PROPERTIES
    ########################

    propertyTriples = [
        (hasLightness, rdfType, owlObjectProperty),
        (hasLightness, rdfsLabel, Literal("hasLightness")),
        (hasLightness, rdfsComment, Literal("Lightness of a color")),
        (hasLightness, rdfsDomain, colorClass),
        (hasLightness, rdfsRange, lightnessClass),
        # Inverse
        (lightnessOf, rdfType, owlObjectProperty),
        (lightnessOf, rdfsLabel, Literal("lightnessOf")),
        (lightnessOf, rdfsComment, Literal("Which colours have a lightness of a given value")),
        (lightnessOf, rdfsDomain, lightnessClass),
        (lightnessOf, rdfsRange, colorClass),
        (lightnessOf, owlInverseProperty, hasLightness),

        (hasSaturation, rdfType, owlObjectProperty),
        (hasSaturation, rdfsComment, Literal("Saturation of a color")),
        (hasSaturation, rdfsDomain, colorClass),
        (hasSaturation, rdfsRange, saturationClass),
        # Inverse
        (saturationOf, rdfType, owlObjectProperty),
        (saturationOf, rdfsLabel, Literal("saturationOf")),
        (saturationOf, rdfsComment, Literal("Which colours have a saturation of a given value")),
        (saturationOf, rdfsDomain, saturationClass),
        (saturationOf, rdfsRange, colorClass),
        (saturationOf, owlInverseProperty, hasSaturation),

        (hasHue, rdfType, owlObjectProperty),
        (hasHue, rdfsComment, Literal("Hue of a color")),
        (hasHue, rdfsDomain, colorClass),
        (hasHue, rdfsRange, hueClass),
        # Inverse
        (hueOf, rdfType, owlObjectProperty),
        (hueOf, rdfsLabel, Literal("hueOf")),
        (hueOf, rdfsComment, Literal("Which colours have a hue of a given value")),
        (hueOf, rdfsDomain, hueClass),
        (hueOf, rdfsRange, colorClass),
        (hueOf, owlInverseProperty, hasHue),
        
        (hasMetallicSheen, rdfType, owlObjectProperty),
        (hasMetallicSheen, rdfsLabel, Literal("hasMetallicSheen")),
        (hasMetallicSheen, rdfsComment, Literal("Metallic Sheen of a color")),
        (hasMetallicSheen, rdfsDomain, colorClass),
        (hasMetallicSheen, rdfsRange, metallicSheenClass),
        # Inverse
        (metallicSheenOf, rdfType, owlObjectProperty),
        (metallicSheenOf, rdfsLabel, Literal("metallicSheenOf")),
        (metallicSheenOf, rdfsComment, Literal("Which colours have an metallic sheen of a given value")),
        (metallicSheenOf, rdfsDomain, metallicSheenClass),
        (metallicSheenOf, rdfsRange, colorClass),
        (metallicSheenOf, owlInverseProperty, hasMetallicSheen),

        # hasGloss
        (hasGloss, rdfType, owlObjectProperty),
        (hasGloss, rdfsLabel, Literal("hasGloss")),
        (hasGloss, rdfsComment, Literal("Gloss of a color")),
        (hasGloss, rdfsDomain, colorClass),
        (hasGloss, rdfsRange, glossClass),
        # Inverse
        (glossOf, rdfType, owlObjectProperty),
        (glossOf, rdfsLabel, Literal("glossOf")),
        (glossOf, rdfsComment, Literal("Which colours have a gloss of a given value")),
        (glossOf, rdfsDomain, glossClass),
        (glossOf, rdfsRange, colorClass),
        (glossOf, owlInverseProperty, hasGloss),

        (hasColorCategory, rdfType, owlObjectProperty),
        (hasColorCategory, rdfsComment, Literal("The 'category' or 'type' of color that a color is perceived to be. The criteria for a color category is that in a given language, the category cannot be said to be a 'type' of any other. E.g. in English, 'Yellow' is not a type of 'Red' or 'Orange', it is a fundamental category. Intermediate colors may be perceived as having multiple 'types' (e.g. turquoise is a type of blue, and a type of green). In different languages, there may be a different number of types. In English and many other languages, there are 11.")),
        (hasColorCategory, rdfsDomain, colorClass),
        (hasColorCategory, rdfsRange, colorCategoryClass),
        # Inverse
        (colorCategoryOf, rdfType, owlObjectProperty),
        (colorCategoryOf, rdfsLabel, Literal("colorCategoryOf")),
        (colorCategoryOf, rdfsComment, Literal("Which colours have a given category")),
        (colorCategoryOf, rdfsDomain, colorCategoryClass),
        (colorCategoryOf, rdfsRange, colorClass),
        (colorCategoryOf, owlInverseProperty, hasColorCategory),

        (hasOpacity, rdfType, owlObjectProperty),
        (hasOpacity, rdfsComment, Literal("Opacity of a color")),
        (hasOpacity, rdfsDomain, colorClass),
        (hasOpacity, rdfsRange, opacityClass),
        # Inverse
        (opacityOf, rdfType, owlObjectProperty),
        (opacityOf, rdfsLabel, Literal("opacityOf")),
        (opacityOf, rdfsComment, Literal("Which colours have an opacity of a given value")),
        (opacityOf, rdfsDomain, opacityClass),
        (opacityOf, rdfsRange, colorClass),
        (opacityOf, owlInverseProperty, hasOpacity),

        # Attributes are members of attribute sets
        (memberOfSet, rdfType, owlObjectProperty),
        (memberOfSet, rdfsLabel, Literal("memberOfSet")),
        (memberOfSet, rdfsComment, Literal("A color attribute is a member of an attribute set.")),
        (memberOfSet, rdfsDomain, colorAttributeClass),
        (memberOfSet, rdfsRange, attributeSetClass),
        # Inverse
        (hasSetMember, rdfType, owlObjectProperty),
        (hasSetMember, rdfsLabel, Literal("hasSetMember")),
        (hasSetMember, rdfsComment, Literal("A color attribute is a member of an attribute set.")),
        (hasSetMember, rdfsDomain, attributeSetClass),
        (hasSetMember, rdfsRange, colorAttributeClass),
        (hasSetMember, owlInverseProperty, memberOfSet),

        # Colours are types of other colours
        (variantOf, rdfType, owlObjectProperty),
        (variantOf, rdfsLabel, Literal("variantOf")),
        (variantOf, rdfsComment, Literal("A color can be a variant of another color.")),
        (variantOf, rdfsDomain, colorClass),
        (variantOf, rdfsRange, colorClass),
        (variantOf, owlEquivalentProperty, dboVariantOf),
        # Inverse
        (hasVariant, rdfType, owlObjectProperty),
        (hasVariant, rdfsLabel, Literal("hasVariant")),
        (hasVariant, rdfsComment, Literal("A color can be a variant of another color.")),
        (hasVariant, rdfsDomain, colorClass),
        (hasVariant, rdfsRange, colorClass),
        (hasVariant, owlEquivalentProperty, dboHasVariant),
        (hasVariant, owlInverseProperty, variantOf),

        # Color attributes have sub-attributes
        (hasSubAttribute, rdfType, owlObjectProperty),
        (hasSubAttribute, rdfsLabel, Literal("hasSubAttribute")),
        (hasSubAttribute, rdfsComment, Literal("A color attribute has a sub-attribute.")),
        (hasSubAttribute, rdfsDomain, colorAttributeClass),
        (hasSubAttribute, rdfsRange, colorAttributeClass),
        # Inverse
        (subAttributeOf, rdfType, owlObjectProperty),
        (subAttributeOf, rdfsLabel, Literal("subAttributeOf")),
        (subAttributeOf, rdfsComment, Literal("A color attribute has a sub-attribute.")),
        (subAttributeOf, rdfsDomain, colorAttributeClass),
        (subAttributeOf, rdfsRange, colorAttributeClass),
        (subAttributeOf, owlInverseProperty, hasSubAttribute),

        # Favourite color! Why not. 
        (hasFavouriteColor, rdfType, owlObjectProperty),
        (hasFavouriteColor, rdfsLabel, Literal("hasFavouriteColor")),
        (hasFavouriteColor, rdfsComment, Literal("A person has a favourite color.")),
        (hasFavouriteColor, rdfsDomain, foafAgent),
        (hasFavouriteColor, rdfsRange, colorClass),
        (hasFavouriteColor, rdfsSubPropertyOf, foafInterest),
        # Inverse
        (favouriteColorOf, rdfType, owlObjectProperty),
        (favouriteColorOf, rdfsLabel, Literal("favouriteColorOf")),
        (favouriteColorOf, rdfsComment, Literal("A person has a favourite color.")),
        (favouriteColorOf, rdfsDomain, colorClass),
        (favouriteColorOf, rdfsRange, foafAgent),
        (favouriteColorOf, owlInverseProperty, hasFavouriteColor),

    ]

    ########################
    # DATA PROPERTIES
    ########################

    dataPropertyTriples = [
        (typicalHsl, rdfType, owlDatatypeProperty),
        (typicalHsl, rdfsComment, Literal("Prototypical HSL of a color")),
        (typicalHsl, rdfsDomain, colorClass),
        (typicalHsl, rdfsRange, xsdString),

        (description, rdfType, owlDatatypeProperty),
        (description, rdfsComment, Literal("Description of a color attribute. Color attributes are meant to be general, not specific.")),
        (description, rdfsDomain, colorAttributeClass),
        (description, rdfsRange, xsdString),

        (name, rdfType, owlDatatypeProperty),
        (name, rdfsComment, Literal("Name of a color")),
        (name, rdfsDomain, colorClass),
        (name, rdfsRange, xsdString),
        (name, owlEquivalentProperty, foafName),

        # Color attributes have preferred desriptors
        (preferredDescriptor, rdfType, owlDatatypeProperty),
        (preferredDescriptor, rdfsComment, Literal("A way of referring to this attribute within a color name. For example a color with an attibute of 'saturated' could be referred to as 'vivid'.")),
        (preferredDescriptor, rdfsDomain, colorAttributeClass),
        (preferredDescriptor, rdfsRange, xsdString),
    ]

    ########################
    # EXTERNAL CLASSES
    ########################

    externalClassTriples = [
        (owlSameAs, rdfType, rdfProperty),
        (rdfType, rdfType, rdfProperty),
        (rdfsLabel, rdfType, rdfProperty),
        (rdfsComment, rdfType, rdfProperty),
        (rdfsSubClassOf, rdfType, rdfProperty),
        (rdfsDomain, rdfType, rdfProperty),
        (rdfsRange, rdfType, rdfProperty),
        (dcTermsTitle, rdfType, rdfProperty),
        (dcTermsCreated, rdfType, rdfProperty),
        (dcTermsModified, rdfType, rdfProperty),
        (owlVersionInfo, rdfType, rdfProperty),
        (dcTermsCreator, rdfType, rdfProperty),
        (vannPreferredNamespacePrefix, rdfType, rdfProperty),
        (sdoCodeRepositoryProperty, rdfType, rdfProperty),
        (dcTermsLicense, rdfType, rdfProperty),
        (dcTermsDescription, rdfType, rdfProperty),
        (skosHistoryNote, rdfType, rdfProperty),
        (rdfsSeeAlso, rdfType, rdfProperty),
        (owlEquivalentProperty, rdfType, rdfProperty),
        (owlEquivalentClass, rdfType, rdfProperty),
        (owlInverseProperty, rdfType, rdfProperty),
        (dboVariantOf, rdfType, rdfProperty),
        (dboHasVariant, rdfType, rdfProperty),
        (foafName, rdfType, rdfProperty),
        (sdoName, rdfType, rdfProperty),
        (sdoEmail, rdfType, rdfProperty),
        (sdoIdentifier, rdfType, rdfProperty),
        (sdoAffiliation, rdfType, rdfProperty),
        (dcTermsContributor, rdfType, rdfProperty),

        # Labels
        (dbpIsccNbs, rdfsLabel, Literal("ISCC–NBS system", datatype=xsdString)),
        (dcTermsContributor, rdfsLabel, Literal("contributor", datatype=xsdString)),
        (dcTermsCreator, rdfsLabel, Literal("creator", datatype=xsdString)),
        (dcTermsPublisher, rdfsLabel, Literal("publisher", datatype=xsdString)),
    ]

    graph.bind(NAMESPACE_PREFIX, cyanNS)
    graph.bind('owl', OWL)
    graph.bind('dcterms', DCTERMS)
    graph.bind('dbp', dbpediaNS)
    graph.bind('dbo', dbpediaOntologyNS)
    graph.bind('skos', SKOS)
    graph.bind('foaf', FOAF)
    graph.bind('schema', SDO)

    for triple in metaTriples:
        graph.add(triple)
    
    for triple in classTriples:
        graph.add(triple)

    for triple in subclassTriples:
        graph.add(triple)

    for triple in propertyTriples:
        graph.add(triple)

    for triple in dataPropertyTriples:
        graph.add(triple)

    for triple in externalClassTriples:
        graph.add(triple)


    return graph





