from rdflib import Namespace
from rdflib.namespace import DCTERMS, OWL, RDF, RDFS, SDO, SKOS, XSD, FOAF

########################
# EXTERNAL NAMESPACES
########################

## RDF http://www.w3.org/1999/02/22-rdf-syntax-ns#
rdfProperty = RDF.Property
rdfStatement = RDF.Statement
rdfType = RDF.type
rdfSubject = RDF.subject
rdfPredicate = RDF.predicate
rdfObject = RDF.object

## RDFS http://www.w3.org/2000/01/rdf-schema#
rdfsResource = RDFS.Resource
rdfsSubClassOf = RDFS.subClassOf
rdfsDomain = RDFS.domain
rdfsComment = RDFS.comment
rdfsRange = RDFS.range
rdfsDefinedBy = RDFS.isDefinedBy
rdfsSeeAlso = RDFS.seeAlso
rdfsLabel = RDFS.label
rdfsSubPropertyOf = RDFS.subPropertyOf

## OWL http://www.w3.org/2002/07/owl#
owlClass = OWL.Class
owlObjectProperty = OWL.ObjectProperty
owlDatatypeProperty = OWL.DatatypeProperty
owlSymmetricProperty = OWL.SymmetricProperty
owlOntology = OWL.Ontology
owlSameAs = OWL.sameAs
owlEquivalentProperty = OWL.equivalentProperty
owlEquivalentClass = OWL.equivalentClass
owlInverseProperty = OWL.inverseOf
owlVersionInfo = OWL.versionInfo
owlDisjointWith = OWL.disjointWith

## XML Schema http://www.w3.org/2001/XMLSchema#
xsdString = XSD.string #xsd:string
xsdDateTime = XSD.dateTime #xsd:dateTime
xsdInteger = XSD.integer #xsd:integer
xsdDecimal = XSD.decimal #xsd:decimal
xsdBoolean = XSD.boolean #xsd:boolean

## DC Terms (Dublin Core Metadata Terms)
# http://purl.org/dc/terms/
dcTermsTitle = DCTERMS.title
dcTermsCreator = DCTERMS.creator
dcTermsDescription = DCTERMS.description
dcTermsCreated = DCTERMS.created
dcTermsModified = DCTERMS.modified
dcTermsLicense = DCTERMS.license
dcTermsContributor = DCTERMS.contributor
dcTermsPublisher = DCTERMS.publisher

## DBPedia http://dbpedia.org/ontology/
DBPEDIA_ONTOLOGY_IRI = "http://dbpedia.org/ontology/"
dbpediaOntologyNS = Namespace(DBPEDIA_ONTOLOGY_IRI)
dboVariantOf = dbpediaOntologyNS["variantOf"]
dboHasVariant = dbpediaOntologyNS["hasVariant"]
dboColorClass = dbpediaOntologyNS["Colour"]

## DBPedia individuals
DBPEDIA_IRI = "http://dbpedia.org/resource/"
dbpediaNS = Namespace(DBPEDIA_IRI)
dbpOpacity = dbpediaNS["Opacity_(optics)"]
dbpIsccNbs = dbpediaNS["ISCC–NBS_system"]

## VANN (Vocabulary and Namespace Notation) http://purl.org/vocab/vann/
VANN_IRI = "http://purl.org/vocab/vann/"
vannNS = Namespace(VANN_IRI)
vannPreferredNamespacePrefix = vannNS["preferredNamespacePrefix"]

## Schema.org http://schema.org/
sdoCodeRepositoryProperty = SDO.codeRepository
sdoName = SDO.name
sdoEmail = SDO.email
sdoIdentifier = SDO.identifier
sdoAffiliation = SDO.affiliation

## SKOS (Simple Knowledge Organization System) http://www.w3.org/2004/02/skos/core#
skosHistoryNote = SKOS.historyNote

# FOAF (Friend of a Friend) http://xmlns.com/foaf/0.1/
foafName = FOAF.name
foafHomepage = FOAF.homepage
foafAgent = FOAF.Agent
foafInterest = FOAF.interest