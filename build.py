from ontology import create_graph
from create_individuals import create_individuals
from create_html import create_html
from http.server import HTTPServer, SimpleHTTPRequestHandler
import os, json, sys
import webbrowser
from rdflib import Namespace
from config import *
from color_helpers import get_closest_color
from color_list import colorList

#########################
# COMMAND LINE ARGUMENTS
#########################
if len(sys.argv) < 2:
    print("Usage: python3 build.py <dev|prod>")
    exit()

# Make sure the argument is either dev or prod
if sys.argv[1] != "dev" and sys.argv[1] != "prod":
    print("Usage: python3 build.py <dev|prod>")
    exit()

# Get the mode (dev/prod)
mode = sys.argv[1]
is_dev = mode == "dev"

# Check if we want to regenerate the colors
regenerate_colors = False
if len(sys.argv) > 2 and sys.argv[2] == "regen":
    regenerate_colors = True

def css_ify_string(string):
    """
    Convert a string to a key that can be used in css
    """
    return string.replace(" ", "-").lower()

if regenerate_colors:
    print("Regenerating colors...")
    # Loop over 24 hues, 11 saturations, and 11 lightnesses
    # And get the closest color to each of the 2904 colors
    hue_index = 0
    saturation_index = 0

    circles = {}
    simplified_color_list = []
    for hue in range(0, 360, 5):
        for saturation in range(0, 100, 4):
            lightness_circle = []
            for lightness in range(5, 100, 10):

                hsl = (hue, saturation, lightness)
                color = get_closest_color(hsl)
                # convert color name for use in css
                color_name = css_ify_string(color)
                lightness_circle.append({
                    "hsl": hsl,
                    "color": {
                        "value": color_name,
                    }
                })
            key = str(hue) + "-" + str(saturation)
            circles[key] = {
                "hue": hue,
                "grid_column": hue_index,
                "grid_row": saturation_index,
                "saturation": saturation,
                "lightness_circle": lightness_circle
            }
            saturation_index += 1
            # If saturation_index is greater than 11, reset it to 0
            if saturation_index > 24:
                saturation_index = 0
        hue_index += 1
        # If hue_index is greater than 24, reset it to 0
        if hue_index > 72:
            hue_index = 0

    # Make simplified version of colorList for json
    for key, value in colorList.items():
        simplified_color_list.append({
            "label": value['info']['name'],
            "key": css_ify_string(value['info']['name']),
            "hsl": value['hsl']
        })
        
    # Print circles object to JSON file
    with open("public/data/color_circles.json", "w") as f:
        json.dump({
            "colors": simplified_color_list,
            "circles": circles
        }, f)

#########################
# BASE URL
#########################
base_url = "https://www.cyannotation.org/"
if is_dev:
    base_url = "http://localhost:8000/"

# Cyannotation namespace
namespace_uri = base_url + "ontology/"
cyanNS = Namespace(namespace_uri)

#########################
# FILEPATHS
#########################
ttl_filepath = "public/rdf/ontology.ttl"
xml_filepath = "public/rdf/ontology.xml"
html_filepath = "public/spec/index.html"

# Create graph
g = create_graph()

# Create individuals
individuals = create_individuals()

# Add individuals to the graph
for triple in individuals:
	g.add(triple)

# Serialize the graph to files
g.serialize(destination=ttl_filepath, format='turtle')
g.serialize(destination=xml_filepath, format="xml")

# Create ontology documentation 
create_html(g, is_dev, base_url)

# If in dev mode, open the html file in a browser
if is_dev:
    PORT = 8000
    os.chdir('public')
    httpd = HTTPServer(('localhost', PORT), SimpleHTTPRequestHandler)
    print('Server started on port:' + str(PORT))
    # Open browser to http://localhost:8000
    webbrowser.open('http://localhost:' + str(PORT))
    httpd.serve_forever()