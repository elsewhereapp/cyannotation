from rdflib import Namespace

BASE_URL = "https://www.cyannotation.org/"
NAMESPACE_URI = BASE_URL + "ontology/"
cyanNS = Namespace(NAMESPACE_URI)
PORT = 8000
LOCAL_URI = "http://localhost:" + str(PORT) + "/"
NAMESPACE_PREFIX = "cyan"
WEBVOWL_ONTOLOGY_IRI_URL = "https://service.tib.eu/webvowl/#iri="
XML_FILEPATH = "rdf/ontology.xml"
TTL_FILEPATH = "rdf/ontology.ttl"
WEBVOWL_VIS_URL = WEBVOWL_ONTOLOGY_IRI_URL + BASE_URL + XML_FILEPATH
CODE_REPOSITORY_URL = "https://gitlab.com/elsewhereapp/cyannotation/"
DESCRIPTION = """Open color ontology, with an convenient (though not definitive) set of instances. 
Cyannotation is based on a set of 'color attribute' classes, from which you can make instances, 
and use those instances to define colors. Current color attributes are mostly based on the HSL 
color scheme. The main goal of cyannotation is the ability to refer to general colors, such as 
'red' or 'dark blue', which do not have a single fixed position in any color space.

The closest thing to this ontology is probably the ISCC–NBS color system. We hope that this 
ontology could also be used to define colors in the ISCC–NBS color system. We have defined less
granular colors than the ISCC-NBS system, in an attempt to simplify the ontology and its applications.

Each color should have a name which matches the level of specificity indicated by its attributes.
E.g. the name 'violet' says something about the hue of the color, but not about the saturation or lightness.
On the other hand 'light violet' says something about the hue and lightness, but not about the saturation.
'vivid light violet' says something about hue, saturation and lightness. Sometimes the color
itself implies a certain attribute, e.g. 'brown' implies low saturation. In that case, that attribute
is regarded as having been specified in the color name. 'Brown' however may cover a wide range of hues, 
from reddish brown to yellowish brown, so it may be important to specify the hue in some way.

The aim of the supplied color set is to provide a 'minimum viable set' of colors that can be used
to describe any color one sees (particularly as an English speaker). If you want to describe
a color that you saw, you should be able to find one in this set and say "it was a color like that". 
"""