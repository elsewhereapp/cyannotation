color_types = {
    "hues": {
        "MAGENTA": {"id": "MagentaHue", "preferredDescriptor": ["Magenta"]},
        "MAGENTA_RED": {"id": "MagentaRedHue", "preferredDescriptor": ["Magenta-Red", "Pink"]},
        "RED": {"id": "RedHue", "preferredDescriptor": ["Red", "Red-Brown"]},
        "ORANGE_RED": {"id": "OrangeRedHue", "preferredDescriptor": ["Orange-Red", "Chocolate Brown"]},
        "ORANGE": {"id": "OrangeHue", "preferredDescriptor": ["Orange", "Wood Brown"]},
        "GOLD_YELLOW": {"id": "GoldenYellowHue", "preferredDescriptor": ["Golden Yellow", "Golden Brown"]},
        "YELLOW": {"id": "YellowHue", "preferredDescriptor": ["Yellow", "Yellow-Brown"]},
        "YELLOW_GREEN": {"id": "YellowGreenHue", "preferredDescriptor": ["Yellow-Green", "Olive Green"]},
        "GREEN": {"id": "GrassGreenHue", "preferredDescriptor": ["Grass Green", "Forest Green"]},
        "SPRING_GREEN": {"id": "SpringGreenHue", "preferredDescriptor": ["Spring Green"]},
        "CYAN": {"id": "CyanHue",  "preferredDescriptor": ["Cyan", "Blue-Green"]},
        "SKY_BLUE": {"id": "SkyBlueHue", "preferredDescriptor": ["Sky Blue", "Aqua", "Cerulean"]},
        "BLUE": {"id": "BlueHue", "preferredDescriptor": ["Royal Blue", "Slate Blue"]},
        "BLUE_PURPLE": {"id": "PurpleBlueHue", "preferredDescriptor": ["Blue-Purple"]},
        "VIOLET": {"id": "VioletHue", "preferredDescriptor": ["Violet"]},

        "NO_HUE": {"id": "NoHue", "preferredDescriptor": []},
        "WARM": {"id": "WarmHue", "preferredDescriptor": []},
        "COOL": {"id": "CoolHue", "preferredDescriptor": []},
    },
    "saturation_name": {
        "SATURATED": {"id": "Saturated", "subAttributeOf": []},
        "DESATURATED": {"id": "Desaturated", "subAttributeOf": []},
        "NO_SATURATION": {"id": "NoSaturation", "subAttributeOf": []},
        "BALANCED_SATURATION": {"id": "BalancedSaturation", "subAttributeOf": []},
        "VERY_HIGH_SATURATION": {"id": "VeryHighSaturation", "subAttributeOf": ["Saturated"]},
        "FAIRLY_HIGH_SATURATION": {"id": "FairlyHighSaturation", "subAttributeOf": ["Saturated"]},
        "VERY_LOW_SATURATION": {"id": "VeryLowSaturation", "subAttributeOf": ["Desaturated"]},
        "FAIRLY_LOW_SATURATION": {"id": "FairlyLowSaturation", "subAttributeOf": ["Desaturated"]},
    },
    "color_temperatures": {
        "COOL": "cool",
        "WARM": "warm",
        "NEUTRAL": "neutral",
        "METALLIC": "metallic",
    }, 
    "lightness": {
        "LIGHT": {"id": "Light", "subAttributeOf": []},
        "MID": {"id": "Mid", "subAttributeOf": []},
        "DARK": {"id": "Dark", "subAttributeOf": []},
        "VERY_DARK": {"id": "VeryDark", "subAttributeOf": ["Dark"]},
        "FAIRLY_DARK": {"id": "FairlyDark", "subAttributeOf": ["Dark"]},
        "VERY_LIGHT": {"id": "VeryLight", "subAttributeOf": ["Light"]},
        "FAIRLY_LIGHT": {"id": "FairlyLight", "subAttributeOf": ["Light"]},
    },
    "metallic_sheen": {
        "METALLIC": "Metallic",
        "NOT_METALLIC": "NotMetallic",
    },
    "color_category": {
        "RED": "RedColorCategory",
        "ORANGE": "OrangeColorCategory",
        "YELLOW": "YellowColorCategory",
        "GREEN": "GreenColorCategory",
        "BLUE": "BlueColorCategory",
        "PURPLE": "PurpleColorCategory",
        "PINK": "PinkColorCategory",
        "GRAY": "GrayColorCategory",
        "WHITE": "WhiteColorCategory",
        "BLACK": "BlackColorCategory",
        "BROWN": "BrownColorCategory",
        "METALLIC": "MetallicColorCategory",
    },
    "opacity": {
        "OPAQUE": "Opaque",
        "FULLY_TRANSPARENT": "FullyTransparent",
        "TRANSLUCENT": "Translucent",
    },
    "gloss": {
        "VERY_MATTE": "VeryMatte",
        "MATTE": "Matte",
        "SEMI_GLOSS": "SemiGloss",
        "GLOSSY": "Glossy",
        "HIGH_GLOSS": "HighGloss",
    },
}

lightness_descriptions = {
        "LIGHT": "A degree of lightness that indicates a color is 'more light than dark'.",
        "MID": "A degree of lightness that indicates a color is 'neither particularly light nor dark",
        "DARK": "A degree of lightness that indicates a color is 'more dark than light'",
        "VERY_DARK": "A degree of lightness that indicates a color is 'very dark'",
        "VERY_LIGHT": "A degree of lightness that indicates a color is 'very light' or 'pale'",
        "FAIRLY_LIGHT": "A degree of lightness that indicates a color is 'fairly light'",
        "FAIRLY_DARK": "A degree of lightness that indicates a color is 'fairly dark'",
        "CUSTOM": "CustomLightness",
}

saturation_descriptions = {
        "SATURATED": "A degree of saturation that indicates a color is 'strongly colored' as opposed to 'dull' or 'pastel'",
        "DESATURATED": "A degree of saturation that indicates a color is 'dull' or 'pastel' as opposed to 'strongly colored'",
        "NO_SATURATION": "A degree of saturation so low that there is no perceivable 'color' (although the saturation may not be technically zero). Colors with this saturation are called 'shades' in English.",
        "BALANCED_SATURATION": "A degree of saturation that indicates a color is neither 'strongly colored' nor 'dull' or 'pastel', but somewhere in between.",
        "VERY_HIGH_SATURATION": "A degree of saturation that is 'very high'. 'Fluoro' colors would fall into this category. Functionally, this could be seen as a more specific type of 'saturated'",
        "FAIRLY_HIGH_SATURATION": "A degree of saturation that is 'fairly high'. Functionally, this could be seen as a more specific type of 'saturated'",
        "VERY_LOW_SATURATION": "A degree of saturation that is 'very low'. Functionally, this could be seen as a more specific type of 'desaturated'",
        "FAIRLY_LOW_SATURATION": "A degree of saturation that is 'fairly low'. Functionally, this could be seen as a more specific type of 'desaturated'",
        "CUSTOM": "CustomSaturation",
}

metallic_sheen_descriptions = {
        "METALLIC": "Some degree of metallic sheen",
        "NOT_METALLIC": "An absence of metallic sheen",
}

opacity_descriptions = {
        "OPAQUE": "A degree of opacity that indicates no visible transparency",
        "FULLY_TRANSPARENT": "A degree of opacity that indicates a color is 'fully transparent', or close enough to appear so. Because it stops no light, no color is visible.",
        "TRANSLUCENT": "A degree of opacity that indicates some degree of transparency. Translucent.",
}

gloss_descriptions = {
        "VERY_MATTE": "A degree of gloss that indicates a color is 'very matte'",
        "MATTE": "A degree of gloss that indicates a color is 'matte'",
        "SEMI_GLOSS": "A degree of gloss that indicates a color is 'semi-gloss'",
        "GLOSSY": "A degree of gloss that indicates a color is 'glossy'",
        "HIGH_GLOSS": "A degree of gloss that indicates a color is 'high-gloss'",
}