color_values = {

  # HUE
  "hue": {
    "MAGENTA": 295,
    "DESAT_MAGENTA": 300,
    "PINK": 335,
    "DESAT_PINK": 330,
    "RED": 0,
    "DESAT_RED": 0,
    "ORANGE_RED": 12,
    "DESAT_ORANGE_RED": 15,
    "ORANGE": 30,
    "DESAT_ORANGE": 30,
    "GOLD_YELLOW": 43,
    "DESAT_GOLDEN_YELLOW": 43,
    "YELLOW": 55,
    "DESAT_YELLOW": 55,
    "YELLOW_GREEN": 73,
    "DESAT_YELLOW_GREEN": 80,
    "GREEN": 115,
    "DESAT_GREEN": 120,
    "SPRING_GREEN": 153,
    "DESAT_SPRING_GREEN": 153,
    "CYAN": 174,
    "DESAT_CYAN": 180,
    "SKY_BLUE": 205,
    "DESAT_SKY_BLUE": 205,
    "BLUE": 230,
    "DESAT_BLUE": 230,
    "BLUE_PURPLE": 254,
    "DESAT_BLUE_PURPLE": 245,
    "PURPLE": 270,
    "DESAT_PURPLE": 270,
    "BLACK": 0,
    "GRAY": 0,
    "WHITE": 0,
    "CREAM": 48,
    "GOLD": 46,
    "SILVER": 255,
    "BRONZE": 30
  },

  # SATURATION
  "saturation": {
    "MAGENTA": 80,
    "DESAT_MAGENTA": 30,
    "PINK": 80,
    "DESAT_PINK": 35,
    "RED": 75,
    "DESAT_RED": 43,
    "ORANGE_RED": 75,
    "DESAT_ORANGE_RED": 40,
    "ORANGE": 90,
    "DESAT_ORANGE": 35,
    "GOLD_YELLOW": 90,
    "DESAT_GOLDEN_YELLOW": 40,
    "YELLOW": 90,
    "DESATURATED_YELLOW": 41,
    "YELLOW_GREEN": 90,
    "DESAT_YELLOW_GREEN": 35,
    "GREEN": 75,
    "DESAT_GREEN": 35,
    "SPRING_GREEN": 80,
    "DESAT_SPRING_GREEN": 40,
    "CYAN": 90,
    "DESAT_CYAN": 40,
    "SKY_BLUE": 82,
    "DESAT_SKY_BLUE": 33,
    "BLUE": 80,
    "DESAT_BLUE": 32,
    "BLUE_PURPLE": 80,
    "DESAT_BLUE_PURPLE": 30,
    "PURPLE": 80,
    "DESAT_PURPLE": 25,
    "BLACK": 0,
    "GRAY": 0,
    "WHITE": 0,
    "CREAM": 65,
    "GOLD": 65,
    "SILVER": 3,
    "BRONZE": 61
  },
    # LIGHTNESS
  "lightness": {

    # Standard lightness values for saturated colors
    "LIGHT": 71,
    "MID": 53,
    "DARK": 38,

    # Standard lightness values for desturated colors
    "DESAT_LIGHT": 64,
    "DESAT_MID": 50,
    "DESAT_DARK": 34,

    # Darker lightness values
    "DARKER_LIGHT": 66,
    "DARKER_MID": 47,
    "DARKER_DARK": 32,

    # Darkest lightness values
    "DARKEST_LIGHT": 64,
    "DARKEST_MID": 45,
    "DARKEST_DARK": 31,

    # Custom lightness values
    "BLACK": 10,
    "WHITE": 96,
    "CREAM": 85,
    "GOLD": 52,
    "SILVER": 67,
    "BRONZE": 50
  }
}
