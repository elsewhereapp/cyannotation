color_names = {
  "simpleColors": {
    "pink": { "name": "Pink", "dbpedia_iri": "http://dbpedia.org/resource/Pink", "dbpediaName": "Pink" },
    "red": { "name": "Red", "dbpedia_iri": "http://dbpedia.org/resource/Red", "dbpediaName": "Red" },
    "brown": { "name": "Brown", "dbpedia_iri": "http://dbpedia.org/resource/Brown", "dbpediaName": "Brown" },
    "orange": { "name": "Orange", "dbpedia_iri": "http://dbpedia.org/resource/Orange_(colour)", "dbpediaName": "Orange (colour)" },
    "yellow": { "name": "Yellow", "dbpedia_iri":   "http://dbpedia.org/resource/Yellow", "dbpediaName": "Yellow" },
    "green": { "name": "Green", "dbpedia_iri": "http://dbpedia.org/resource/Green", "dbpediaName": "Green" },
    "blue": { "name": "Blue", "dbpedia_iri": "http://dbpedia.org/resource/Blue", "dbpediaName": "Blue" },
    "purple": { "name": "Purple", "dbpedia_iri": "http://dbpedia.org/resource/Purple", "dbpediaName": "Purple" },
    "black": { "name": "Black", "dbpedia_iri": "http://dbpedia.org/resource/Black", "dbpediaName": "Black" },
    "white": { "name": "White", "dbpedia_iri": "http://dbpedia.org/resource/White", "dbpediaName": "White" },
    "gray": { "name": "Gray", "dbpedia_iri": "http://dbpedia.org/resource/Grey", "dbpediaName": "Grey" },
    "magenta": { "name": "Magenta", "dbpedia_iri": "http://dbpedia.org/resource/Magenta", "dbpediaName": "Magenta" },
    "cyan": { "name": "Cyan", "dbpedia_iri": "http://dbpedia.org/resource/Cyan", "dbpediaName": "Cyan" },
    "oliveGreen": { "name": "Olive Green", "dbpedia_iri": "http://dbpedia.org/resource/Olive_(color)", "dbpediaName": "Olive (color)" },
    "yellowGreen": { "name": "Yellow Green", "dbpedia_iri": "", "dbpediaName": "" },
  },
  "complexColors": {
    "vividLightMagenta": { "name": "Vivid Light Magenta", "dbpedia_iri": "http://dbpedia.org/resource/Orchid_(color)", "dbpediaName": "Orchid (color)" }, # Orchid
    "vividMidMagenta": { "name": "Vivid Mid Magenta", "dbpedia_iri": "http://dbpedia.org/resource/Magenta", "dbpediaName": "Magenta" }, # Magenta
    "vividDarkMagenta": { "name": "Vivid Dark Magenta", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyLightMagenta": { "name": "Dusky Light Magenta", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_pink__Grayish_purplish_pink__1", "dbpediaName": "Pink Lavender" }, # Pink Lavender
    "duskyMidMagenta": { "name": "Dusky Mid Magenta", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_violet__Moderate_purple__1", "dbpediaName": "Chinese Violet" }, # Chinese Violet
    "duskyDarkMagenta": { "name": "Dusky Dark Magenta", "dbpedia_iri": "http://dbpedia.org/resource/Eggplant_(color)", "dbpediaName": "Eggplant (color)" }, # "Eggplant" (seems to be closest to this)pad.katalyst.com.au
 
    "vividLightPink": { "name": "Vivid Light Pink", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_pink__Moderate_pink__1", "dbpediaName": "Light Pink" },
    "vividMidMagentaRed": { "name": "Vivid Mid Magenta-Red", "dbpedia_iri": "http://dbpedia.org/resource/Rose_(color)", "dbpediaName": "Rose (color)" },
    "vividDarkMagentaRed": { "name": "Vivid Dark Magenta-Red", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_magenta__Deep_purplish_red__1", "dbpediaName": "Quinacridone Magenta" },
    "duskyLightPink": { "name": "Dusky Light Pink", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_pink__Pale_purplish_pink__1", "dbpediaName": "Fairy Tale" },
    "duskyMidMagentaRed": { "name": "Dusky Mid Magenta-Red", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_purple__Deep_purplish_pink__1", "dbpediaName": "Liseran Purple" },
    "duskyDarkMagentaRed": { "name": "Dusky Dark Magenta-Red", "dbpedia_iri": "http://dbpedia.org/resource/Wine_(color)", "dbpediaName": "Wine (color)" },

    "vividLightRed": { "name": "Vivid Light Red", "dbpedia_iri": "http://dbpedia.org/resource/Salmon_(color)", "dbpediaName": "Salmon (color)" },
    "vividMidRed": { "name": "Vivid Mid Red", "dbpedia_iri": "http://dbpedia.org/resource/Fire_engine_red", "dbpediaName": "Fire engine red" },
    "vividDarkRed": { "name": "Vivid Dark Red", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_red__Dark_red__1", "dbpediaName": "Cordovan" },
    "lightRedBrown": { "name": "Light Red-Brown", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_red__Light_grayish_red__1", "dbpediaName": "Rosy brown" },
    "midRedBrown": { "name": "Mid Red-Brown", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_red__Moderate_red__1", "dbpediaName": "Indian Red" },
    "darkRedBrown": { "name": "Dark Red-Brown", "dbpedia_iri": "http://dbpedia.org/resource/Maroon", "dbpediaName": "Maroon" },

    "vividLightOrangeRed": { "name": "Vivid Light Orange-Red", "dbpedia_iri": "http://dbpedia.org/resource/Apricot_(color)", "dbpediaName": "Apricot (color)" },
    "vividMidOrangeRed": { "name": "Vivid Mid Orange-Red", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_red__Vivid_reddish_orange__1", "dbpediaName": "Syracuse Orange" },
    "vividDarkOrangeRed": { "name": "Vivid Dark Orange-Red", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_orange__Deep_reddish_orange__1", "dbpediaName": "Burnt orange" },
    "lightChocolateBrown": { "name": "Light Chocolate Brown", "dbpedia_iri": "", "dbpediaName": "" },
    "midChocolateBrown": { "name": "Mid Chocolate Brown", "dbpedia_iri": "", "dbpediaName": "" },
    "darkChocolateBrown": { "name": "Dark Chocolate Brown", "dbpedia_iri": "", "dbpediaName": "" },

    "vividLightOrange": { "name": "Vivid Light Orange", "dbpedia_iri": "http://dbpedia.org/resource/Sunset_(color)", "dbpediaName": "Sunset (color)" },
    "vividMidOrange": { "name": "Vivid Mid Orange", "dbpedia_iri": "http://dbpedia.org/resource/Safety_orange", "dbpediaName": "Safety orange" },
    "vividDarkOrange": { "name": "Vivid Dark Orange", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_orange__Deep_orange__1", "dbpediaName": "Alloy orange" },
    "lightWoodBrown": { "name": "Light Wood Brown", "dbpedia_iri": "", "dbpediaName": "" },
    "midWoodBrown": { "name": "Mid Wood Brown", "dbpedia_iri": "", "dbpediaName": "" },
    "darkWoodBrown": { "name": "Dark Wood Brown", "dbpedia_iri": "", "dbpediaName": "" },

    "lightGoldenYellow": { "name": "Light Golden Yellow", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_yellow__Brilliant_yellow__1", "dbpediaName": "Royal Yellow" },
    "midGoldenYellow": { "name": "Mid Golden Yellow", "dbpedia_iri": "http://dbpedia.org/resource/Amber_(color)", "dbpediaName": "Amber (color)" },
    "darkGoldenYellow": { "name": "Dark Golden Yellow", "dbpedia_iri": "", "dbpediaName": "" },
    "lightGoldenBrown": { "name": "Light Golden Brown", "dbpedia_iri": "", "dbpediaName": "" },
    "midGoldenBrown": { "name": "Mid Golden Brown", "dbpedia_iri": "", "dbpediaName": "" },
    "darkGoldenBrown": { "name": "Dark Golden Brown", "dbpedia_iri": "", "dbpediaName": "" },

    "vividLightYellow": { "name": "Vivid Light Yellow", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_yellow__Brilliant_greenish_yellow__1", "dbpediaName": "Unmellow Yellow" },
    "vividMidYellow": { "name": "Vivid Mid Yellow", "dbpedia_iri": "http://dbpedia.org/resource/Lemon_(color)", "dbpediaName": "Lemon (color)" },
    "vividDarkYellow": { "name": "Vivid Dark Yellow", "dbpedia_iri": "", "dbpediaName": "" },
    "lightYellowBrown": { "name": "Light Yellow-Brown", "dbpedia_iri": "http://dbpedia.org/resource/Straw_(colour)", "dbpediaName": "Straw (colour)" },
    "midYellowBrown": { "name": "Mid Yellow-Brown", "dbpedia_iri": "", "dbpediaName": "" },
    "darkYellowBrown": { "name": "Dark Yellow-Brown", "dbpedia_iri": "", "dbpediaName": "" },

    "vividLightYellowGreen": { "name": "Vivid Light Yellow-Green", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_yellow__Brilliant_yellow_green__1", "dbpediaName": "Mindaro" },
    "vividMidYellowGreen": { "name": "Vivid Mid Yellow-Green", "dbpedia_iri": "http://dbpedia.org/resource/Chartreuse_(color)", "dbpediaName": "Chartreuse (color)" },
    "vividDarkYellowGreen": { "name": "Vivid Dark Yellow-Green", "dbpedia_iri": "", "dbpediaName": "" },
    "lightOliveGreen": { "name": "Light Olive Green", "dbpedia_iri": "http://dbpedia.org/resource/Olive_(color)__Moderate_yellow_green__1", "dbpediaName": "Olivine" },
    "midOliveGreen": { "name": "Mid Olive Green", "dbpedia_iri": "", "dbpediaName": "" },
    "darkOliveGreen": { "name": "Dark Olive Green", "dbpedia_iri": "http://dbpedia.org/resource/Olive_(color)__Moderate_olive_green__1", "dbpediaName": "Dark Olive Green" },

    "lightGrassGreen": { "name": "Light Grass Green", "dbpedia_iri": "", "dbpediaName": "" },
    "midGrassGreen": { "name": "Mid Grass Green", "dbpedia_iri": "", "dbpediaName": "" },
    "darkGrassGreen": { "name": "Dark Grass Green", "dbpedia_iri": "", "dbpediaName": "" },
    "lightForestGreen": { "name": "Light Forest Green", "dbpedia_iri": "", "dbpediaName": "" },
    "midForestGreen": { "name": "Mid Forest Green", "dbpedia_iri": "", "dbpediaName": "" },
    "darkForestGreen": { "name": "Dark Forest Green", "dbpedia_iri": "", "dbpediaName": "" },

    "vividLightSpringGreen": { "name": "Vivid Light Spring Green", "dbpedia_iri": "", "dbpediaName": "" },
    "vividMidSpringGreen": { "name": "Vivid Mid Spring Green", "dbpedia_iri": "", "dbpediaName": "" },
    "vividDarkSpringGreen": { "name": "Vivid Dark Spring Green", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyLightSpringGreen": { "name": "Dusky Light Spring Green", "dbpedia_iri": "http://dbpedia.org/resource/Cambridge_Blue_(colour)", "dbpediaName": "Cambridge Blue (colour)" },
    "duskyMidSpringGreen": { "name": "Dusky Mid Spring Green", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyDarkSpringGreen": { "name": "Dusky Dark Spring Green", "dbpedia_iri": "", "dbpediaName": "" },

    "vividLightBlueGreen": { "name": "Vivid Light Blue-Green", "dbpedia_iri": "", "dbpediaName": "" },
    "vividMidBlueGreen": { "name": "Vivid Mid Blue-Green", "dbpedia_iri": "http://dbpedia.org/resource/Turquoise_(color)", "dbpediaName": "Turquoise (color)" },
    "vividDarkBlueGreen": { "name": "Vivid Dark Blue-Green", "dbpedia_iri": "http://dbpedia.org/resource/Teal", "dbpediaName": "Teal" },
    "duskyLightBlueGreen": { "name": "Dusky Light Blue-Green", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyMidBlueGreen": { "name": "Dusky Mid Blue-Green", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyDarkBlueGreen": { "name": "Dusky Dark Blue-Green", "dbpedia_iri": "", "dbpediaName": "" },

    "vividLightSkyBlue": { "name": "Vivid Light Sky Blue", "dbpedia_iri": "", "dbpediaName": "" },
    "vividMidSkyBlue": { "name": "Vivid Mid Sky Blue", "dbpedia_iri": "", "dbpediaName": "" },
    "vividDarkSkyBlue": { "name": "Vivid Dark Sky Blue", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyLightCerulean": { "name": "Dusky Light Cerulean", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyMidCerulean": { "name": "Dusky Light Cerulean", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyDarkCerulean": { "name": "Dusky Dark Cerulean", "dbpedia_iri": "", "dbpediaName": "" },

    "lightRoyalBlue": { "name": "Light Royal Blue", "dbpedia_iri": "http://dbpedia.org/resource/Cornflower_blue", "dbpediaName": "Cornflower blue" },
    "midRoyalBlue": { "name": "Mid Royal Blue", "dbpedia_iri": "", "dbpediaName": "" },
    "darkRoyalBlue": { "name": "Dark Royal Blue", "dbpedia_iri": "", "dbpediaName": "" },
    "lightSlateBlue": { "name": "Light Slate Blue", "dbpedia_iri": "", "dbpediaName": "" },
    "midSlateBlue": { "name": "Mid Slate Blue", "dbpedia_iri": "", "dbpediaName": "" },
    "darkSlateBlue": { "name": "Dark Slate Blue", "dbpedia_iri": "", "dbpediaName": "" },

    "vividLightBluePurple": { "name": "Vivid Light Blue-Purple", "dbpedia_iri": "", "dbpediaName": "" },
    "vividMidBluePurple": { "name": "Vivid Mid Blue-Purple", "dbpedia_iri": "", "dbpediaName": "" },
    "vividDarkBluePurple": { "name": "Vivid Dark Blue-Purple", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyLightBluePurple": { "name": "Dusky Light Blue-Purple", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyMidBluePurple": { "name": "Dusky Mid Blue-Purple", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyDarkBluePurple": { "name": "Dusky Dark Blue-Purple", "dbpedia_iri": "", "dbpediaName": "" },

    "vividLightViolet": { "name": "Vivid Light Violet", "dbpedia_iri": "http://dbpedia.org/resource/Mauve", "dbpediaName": "Mauve" },
    "vividMidViolet": { "name": "Vivid Mid Violet", "dbpedia_iri": "http://dbpedia.org/resource/Violet_(color)", "dbpediaName": "Violet (color)" },
    "vividDarkViolet": { "name": "Vivid Dark Violet", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyLightViolet": { "name": "Light Dusky Violet", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyMidViolet": { "name": "Mid Dusky Violet", "dbpedia_iri": "", "dbpediaName": "" },
    "duskyDarkViolet": { "name": "Dark Dusky Violet", "dbpedia_iri": "http://dbpedia.org/resource/Shades_of_violet__Dark_purple__1", "dbpediaName": "English Violet" },

    "jetBlack": { "name": "Jet Black", "dbpedia_iri": "", "dbpediaName": "" },

    "lightGray": { "name": "Light Gray", "dbpedia_iri": "", "dbpediaName": "" },
    "midGray": { "name": "Mid Gray", "dbpedia_iri": "", "dbpediaName": "" },
    "darkGray": { "name": "Dark Gray", "dbpedia_iri": "", "dbpediaName": "" },

    "snowWhite": { "name": "Snow White", "dbpedia_iri": "", "dbpediaName": "" },
    "cream": { "name": "Cream", "dbpedia_iri": "", "dbpediaName": "" },

    "gold": { "name": "Gold", "dbpedia_iri": "", "dbpediaName": "" },
    "silver": { "name": "Silver", "dbpedia_iri": "", "dbpediaName": "" },
    "bronze": { "name": "Bronze", "dbpedia_iri": "", "dbpediaName": "" },
  }
}
