from config import *

# Get class/instance name from URL
def get_name_from_url(instance_url):
    if not instance_url:
        return ""

    # First check if the instance_url contains a #
    # If it does, return the part after the #
    if '#' in instance_url:
        return instance_url.split('#')[1]

    # If it doesn't, return the last part of the url
    if "/" in instance_url:
        # If the url ends with a /, return the part before the last /
        if instance_url.endswith("/"):
            return instance_url.split("/")[-2]
        return instance_url.split('/')[-1]

    # In this case, it's probably a literal
    return instance_url

# Get namespace from URL
def get_namespace_from_url(instance_url):
    if not instance_url:
        return ""
    name = get_name_from_url(instance_url)
    # If the name is empty, return empty string
    if not name:
        return ""
    return instance_url.replace(name, "")

# Check if string is a URL
def is_url(string):
    # if string is empty, return false
    if not string:
        return False
    if string.startswith("http"):
        return True
    return False

# If string is a URL, return the URL
# If string is not a URL, return empty string
def get_url(string, is_dev):
    if is_url(string):
        # If in dev mode, replace BASE_URL with LOCAL_URI
        if is_dev:
            return string.replace(BASE_URL, LOCAL_URI)
        else:
            return string
    else:
        return ""