function hslValuesToString(h, s, l) {
  return `hsl(${h}, ${s}%, ${l}%)`;
}

document.addEventListener("DOMContentLoaded", function () {
  console.log("DOM loaded");
  // Get color-circles-container div
  var circlesContainer = document.getElementById("color-circles-container");

  if (circlesContainer) {
    // Polyfill for Object.values
    if (!Object.values) {
      Object.values = function (obj) {
        var vals = [];
        for (var key in obj) {
          if (obj.hasOwnProperty(key)) {
            vals.push(obj[key]);
          }
        }
        return vals;
      };
    }

    // Read JSON from ..data/color_circles.json
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "../../data/color_circles.json", false);
    xhr.send(null);
    var colorGridData = JSON.parse(xhr.responseText);

    // Get values from circles object (Object.values)
    var circlesValues = Object.values(colorGridData.circles);

    var colors = colorGridData.colors;
    // Map over the colors and make a button for each
    var colorButtons = colors.map(function (color) {
      var button = document.createElement("button");
      button.classList.add("color-button");
      button.style.backgroundColor = hslValuesToString(...color.hsl);
      console.log(hslValuesToString(color.hsl));
      button.addEventListener("mouseover", function () {
        findColor(color.key);
      });
      return button;
    });
    // Append buttons to buttons-container
    var buttonsContainer = document.getElementById("buttons-container");
    colorButtons.forEach(function (button) {
      buttonsContainer.appendChild(button);
    });

    // Loop through all circles
    // Create a new div for each circle inside color-circles-container
    for (var i = 0; i < circlesValues.length; i++) {
      var circle = circlesValues[i];
      var circleSegments = circle.lightness_circle;

      var nineByNine = document.createElement("div");
      nineByNine.className = "lightness-circle";
      // circleDiv.style.backgroundColor = circle.color;

      // Put it in the correct position in the grid
      nineByNine.style.gridColumnStart = circle.grid_column + 1;
      nineByNine.style.gridColumnEnd = circle.grid_column + 2;
      nineByNine.style.gridRowStart = circle.grid_row + 1;
      nineByNine.style.gridRowEnd = circle.grid_row + 2;

      circlesContainer.appendChild(nineByNine);

      // Create a new div for each segment
      for (var j = 0; j < circleSegments.length; j++) {
        var circleSegment = circleSegments[j];
        var circleSegmentColor = circleSegment.hsl;
        var circleSegmentColorHSL = hslValuesToString(...circleSegmentColor);

        // Create a new div
        var circleSegmentDiv = document.createElement("div");
        circleSegmentDiv.className = "circle-segment";
        // Set the background color of the div
        circleSegmentDiv.style.backgroundColor = circleSegmentColorHSL;
        // Add data-color attribute to the div
        circleSegmentDiv.setAttribute("data-color", circleSegment.color.value);

        // Append the div to the circle div
        nineByNine.appendChild(circleSegmentDiv);
      }
    }
  }
});

function findColor(colorKey) {
  // Go through each .circle-segment div
  var circleSegments = document.querySelectorAll(".circle-segment");
  for (var i = 0; i < circleSegments.length; i++) {
    // Get the data-color attribute of the div
    var circleSegmentColor = circleSegments[i].getAttribute("data-color");
    // If the data-color attribute is vivid-mid-red, show the div
    // else, hide the div
    if (circleSegmentColor === colorKey) {
      circleSegments[i].style.display = "block";
    } else {
      circleSegments[i].style.display = "none";
    }
  }
}
