from ontology import *
from color_list import *
import os, jinja2
from color_names import *
from color_types import *
from color_values import *
from helpers_url import *
from config import *
from helpers_queries import *

# We make sure that properties with many values are at the bottom of the page
def sortFunction(x):
    return len(x["vals"])

# If a property has multiple values, consolidate them
def consolidate_rows(rows):
    row_obj = {}
    for row in rows:
        row_property_id = row["property"]["id"]
        if row_property_id in row_obj:
            row_obj[row_property_id]["vals"].append(row["value"])
        else:
            row_obj[row_property_id] = {
                "property": row["property"],
                "vals": [row["value"]]
            }
    new_rows = []
    for key in row_obj:
        if row_obj[key]:
            new_rows.append(row_obj[key])

    sortedRows = sorted(new_rows, key=sortFunction, reverse=False)

    return sortedRows


PATH_TO_ONTOLOGY = 'public/ontology/'
# Template vars
# https://stackoverflow.com/questions/38642557/how-to-load-jinja-template-directly-from-filesystem
templateLoader = jinja2.FileSystemLoader(searchpath="./_templates")
templateEnv = jinja2.Environment(loader=templateLoader)
PAGE_TEMPLATE_FILE = "page.html"
HOMEPAGE_TEMPLATE_FILE = "homepage.html"
COLOR_FINDER_TEMPLATE_FILE = "name-a-color.html"
page_template = templateEnv.get_template(PAGE_TEMPLATE_FILE)
homepage_template = templateEnv.get_template(HOMEPAGE_TEMPLATE_FILE)
color_finder_template = templateEnv.get_template(COLOR_FINDER_TEMPLATE_FILE)

def create_html(g, is_dev, base_url): 

    print("Generating HTML...")

    #########################
    # PROPERTIES
    #########################
    properties = get_properties(g, is_dev)

    # Divide the properties into object and datatype properties
    object_properties = []
    datatype_properties = []
    for property_ in properties:
        if property_["type"]["label"] == "ObjectProperty":
            object_properties.append(property_)
        else:
            datatype_properties.append(property_)
    
    for propertyRow in properties:
        propertyName = propertyRow["name"]
        propertyType = propertyRow["type"]
        propertyComment = propertyRow["comment"]
        propertyProperties = get_properties_of_resource(propertyRow['iri'], g, is_dev)

        # Make a folder for the property in the 'ontology' folder
        os.mkdir(PATH_TO_ONTOLOGY + propertyName)
        # Make an index.html in the folder, describing the instance
        with open(PATH_TO_ONTOLOGY + propertyName + '/index.html', 'w') as f:
            full_html = page_template.render(iri=word_breakify(propertyRow["iri"]), resource=propertyRow, title=propertyName, description=propertyName, baseUrl=base_url, instanceClass=propertyType, comment=propertyComment, rows=consolidate_rows(propertyProperties))
            f.write(full_html)

    #########################
    # CLASSES
    #########################
    classResults = get_classes(g, is_dev)
    instances = {}

    for classRow in classResults:
        className = classRow['id']
        classComment = classRow['comment']
        classIri = classRow['iri']
        classType = classRow['type']
        classProperties = get_properties_of_resource(classIri, g, is_dev)
        instanceRes = get_instances_of_class(classIri, g, is_dev)

        ##################################
        # Instances
        ##################################
        for instanceRow in instanceRes:

            instance = instanceRow['iri']
            instanceId = instanceRow['id']
            instanceName = instanceRow['name']
            instanceClass = instanceRow['type']
            hslValue = instanceRow['color'] # Optional color

            # Get all properties of the instance 
            properties = get_properties_of_resource(instance, g, is_dev)

            instanceDetails = {
                "href": get_url(instance, is_dev),
                "id": instanceId,
                "name": instanceName,
                "type": instanceClass,
                "color": hslValue,
            }

            # Add instance to the instance object, based on its class
            if instanceClass['label'] in instances:
                instances[instanceClass['label']].append(instanceDetails)
            else:
                instances[instanceClass['label']] = [instanceDetails]

            # Make a folder for the instance in the 'ontology' folder
            os.mkdir(PATH_TO_ONTOLOGY + instanceId)
            # Make an index.html in the folder, describing the instance
            with open(PATH_TO_ONTOLOGY + instanceId + '/index.html', 'w') as f:
                full_html = page_template.render(iri=word_breakify(instanceRow["iri"]), resource=instanceRow, title=instanceName, description=instanceId, baseUrl=base_url, instanceClass=instanceClass, rows=consolidate_rows(properties), hslValue=hslValue)
                f.write(full_html)

        # Make a folder for the class in the 'ontology' folder
        os.mkdir(PATH_TO_ONTOLOGY + className)
        # Make an index.html in the folder, describing the instance
        with open(PATH_TO_ONTOLOGY + className + '/index.html', 'w') as f:
            full_html = page_template.render(iri=word_breakify(classRow["iri"]), resource=classRow, title=className, description=className, baseUrl=base_url, instanceClass=classType, comment=classComment, members=instanceRes, rows=consolidate_rows(classProperties))
            f.write(full_html)

    ##################################
    # Get ontology details
    ##################################

    # Get ontology details
    ontologyMetadata = get_properties_of_resource(cyanNS, g, is_dev)

    contributorRoles = get_contys(g, cyanNS, is_dev)
    consolidatedMetadata = consolidate_rows(ontologyMetadata)

    # print(consolidatedMetadata)



    # Filter out the creator, publisher, and contributor roles from consolidatedMetadata
    # Since they are in the contributorRoles
    sanitisedMetadata = []
    for row in consolidatedMetadata:
        # Remove the creator, publisher, and contributor properties
        if (row["property"]["name"]) in ["creator", "publisher", "contributor"]:
            continue
        sanitisedMetadata.append(row)


    # print the name of each contributor
    # for contributor in contributors:
    #     print(contributor['contributor']['name'])

    print("Generating homepage...")

    # Turn instances dictionary into an array and sort by number of instances
    instancesArray = []
    for key in instances:
        instancesArray.append({
            "label": key,
            "count": len(instances[key]),
            "instances": instances[key]
        })
    instancesArray = sorted(instancesArray, key=lambda k: k['count'], reverse=True)

    # Print homepage HTML
    homepage_html = homepage_template.render(contributorRoles=contributorRoles, webVowlUrl=WEBVOWL_VIS_URL, ttlFilepath=TTL_FILEPATH, xmlFilepath=XML_FILEPATH, title='Cyannotation', instances=instancesArray, description=description, isHomepage=True, baseUrl=base_url, homepage_classes=classResults, homepage_data_properties=datatype_properties, homepage_object_properties=object_properties, rows=sanitisedMetadata, ontologyIri=cyanNS)
    with open('public/' + 'index.html', 'w') as f:
        f.write(homepage_html)

    # Print Color finder HTML
    color_finder_html = color_finder_template.render(isColorPage=True, baseUrl=base_url, title='Color Finder', description=description, isHomepage=False)
    with open('public/tools/name-a-color/' + 'index.html', 'w') as f:
        f.write(color_finder_html)

