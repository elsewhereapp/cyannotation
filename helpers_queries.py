from config import *
from namespace_definitions import *
from helpers_url import *
from rdflib.namespace import SDO, FOAF, DCTERMS

# Query to get the label of a resource
def get_label(resourceId, resourceOntologyIri, graph):
    # If there's no resourceOntologyIri, it's a literal, just return the resourceId
    if not resourceOntologyIri:
        return resourceId

    # Make iri from resourceOntologyIri and resourceId
    iri = resourceOntologyIri + resourceId

    labelQuery = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    SELECT DISTINCT ?label
    WHERE {{
        <{iri}> rdfs:label ?label .
    }}
    """.format(iri=iri)

    results = graph.query(labelQuery)
    res = ""
    # We're only expecting one result
    for result in results:
        res = result[0]

    # If there's no label, return the resourceId
    if not res:
        return resourceId

    # Return the label
    return res


def create_resource_obj(iriOrLiteral, labelString, commentString, colorString, type, is_dev):

    # id
    id = get_name_from_url(iriOrLiteral)

    # label
    label = id
    if labelString:
        label = labelString.strip()

    # comment
    comment = ""
    if commentString:
        comment = commentString.strip()

    # color
    color = ""
    if colorString:
        color = colorString.strip()
    
    # namespace
    namespace = get_namespace_from_url(iriOrLiteral)

    # href
    # Check if iriOrLiteral is a URL
    href = ""
    if is_url(iriOrLiteral):
        href = get_url(iriOrLiteral, is_dev)
    else:
        href = ""
    # If href doesn't have a trailing slash, add one
    if href and not href.endswith("/"):
        # If the url contains a hash, ignore
        if not "#" in href:
            href += "/"

    # is_external
    is_external = False
    # If the namespace is not the NAMESPACE_URI, it's an external link
    if namespace != NAMESPACE_URI and not href == "":
        is_external = True

    return {
        "id": id,
        "name": label, # Should be 'label'
        "namespace": namespace,
        "href": href,
        "iri": iriOrLiteral,
        "is_external": is_external,
        "comment": comment,
        "color": color,
        "type": {
            "label": get_name_from_url(type),
            "href": type
        }
    }


# Get all classes in the ontology
def get_classes(graph, is_dev):
    # Get all classes
    classesQuery = """
    SELECT DISTINCT ?class ?classLabel ?classComment ?type
    WHERE {{
        ?class a owl:Class .
        ?class a ?type .
        OPTIONAL {{ ?class rdfs:label ?classLabel . }}
        OPTIONAL {{ ?class rdfs:comment ?classComment . }}
    }}
    """.format(ontology_iri=NAMESPACE_URI)

    results = graph.query(classesQuery)
    classes = []
    # For each result, create a class object
    for result in results:
        class_ = result[0]
        classLabel = result[1]
        classComment = result[2]
        type = result[3]
        classObj = create_resource_obj(class_, classLabel, classComment,  "", type, is_dev)
        classes.append(classObj)

    return classes

# Get all properties in the ontology
def get_properties(graph, is_dev):
    # Get all properties
    propertiesQuery = """
    SELECT DISTINCT ?property ?propertyLabel ?propertyComment ?type
    WHERE {{
        {{ ?property a owl:DatatypeProperty }}
        UNION
        {{ ?property a owl:ObjectProperty }}
        ?property a ?type .
        OPTIONAL {{ ?property rdfs:label ?propertyLabel . }}
        OPTIONAL {{ ?property rdfs:comment ?propertyComment . }}
    }}
    """.format(ontology_iri=NAMESPACE_URI)

    results = graph.query(propertiesQuery)
    properties = []
    # For each result, create a property object
    for result in results:
        property_ = result[0]
        propertyLabel = result[1]
        propertyComment = result[2]
        type = result[3]
        property_ = create_resource_obj(property_, propertyLabel, propertyComment,  "", type, is_dev)
        properties.append(property_)

    return properties

# HTMLify a string
def htmlify(string):
    return string.replace("\n\n", "<br><br>")

def word_breakify(string):
    return string.replace("/", "<wbr>/")


# We could do this smarter (include it in the properties query)
# but it's not that important for now
def get_contys(graph, ontologyIri, is_dev):

    rows = []

    contributorQuery = """
    PREFIX dct: <{dct}>
    PREFIX sdo: <{sdo}>
    PREFIX foaf: <{foaf}>
    SELECT DISTINCT ?contributor ?role ?contributorName ?contributorEmail ?contributorIdentifier ?contributorAffiliation ?contributorHomepage
    WHERE {{
        {{ <{ontologyIri}> dct:publisher ?contributor . }}
        UNION
        {{ <{ontologyIri}> dct:creator ?contributor . }}
        UNION
        {{ <{ontologyIri}> dct:contributor ?contributor . }}
        <{ontologyIri}> ?role ?contributor .
        ?contributor sdo:name ?contributorName .
        OPTIONAL {{ ?contributor sdo:identifier ?contributorIdentifier . }}
        OPTIONAL {{ ?contributor foaf:homepage ?contributorHomepage . }}
        OPTIONAL {{ ?contributor sdo:email ?contributorEmail . }}
        OPTIONAL {{ ?contributor sdo:affiliation ?contributorAffiliation . }}
    }}
    """.format(ontologyIri=ontologyIri, sdo=SDO, foaf=FOAF, dct=DCTERMS)

    results = graph.query(contributorQuery)
    # For each result, create a contributor object
    res = {}
    for result in results:
        contributorRole = result[1]
        contributorName = result[2]
        contributorEmail = result[3]
        contributorIdentifier = result[4]
        contributorAffiliation = result[5]
        contributorHomepage = result[6]
        # contributorFaveColor = result[7]
        # contributorFaveColorHsl = result[8]
        contributorRoleName = get_name_from_url(contributorRole)

        # def create_resource_obj(iriOrLiteral, labelString, commentString, colorString, type, is_dev):
        role = create_resource_obj(contributorRole, "", "", "", "", is_dev)
        name = create_resource_obj(contributorName, "", "", "", "", is_dev)
        email = create_resource_obj(contributorEmail, "", "", "", "", is_dev)
        identifier = create_resource_obj(contributorIdentifier, "", "", "", "", is_dev)
        affiliation = create_resource_obj(contributorAffiliation, "", "", "", "", is_dev)
        homepage = create_resource_obj(contributorHomepage, "", "", "", "", is_dev)
        # faveColor = create_resource_obj(contributorFaveColor, "", "", contributorFaveColorHsl, "", is_dev)


        contributor = {
            "name": name,
            "email": email,
            "identifier": identifier,
            "affiliation": affiliation,
            "homepage": homepage,
            # "faveColor": faveColor
        }

        # Now group by role
        # role = result[1]
        if not contributorRoleName in res:
            res[contributorRoleName] = {
                "property": role,
                "contributors": [contributor]
            }
        else:
            res[contributorRoleName]["contributors"].append(contributor)

    # Now convert res to an array
    for key in res:
        rows.append(res[key])
    
    return rows


# Get all instances of a class
def get_instances_of_class(class_, graph, is_dev):
    # Get all instances of the class
    instancesQuery = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX {namespace_prefix}: <{namespace_uri}>
    SELECT DISTINCT ?instance ?instanceLabel ?optionalColor ?type
    WHERE {{
        ?instance a <{class_}> .
        ?instance a ?type .
        OPTIONAL {{ ?instance rdfs:label ?instanceLabel . }}
        OPTIONAL {{ ?instance {namespace_prefix}:typicalHsl ?optionalColor . }}
    }}
    """.format(class_=class_, namespace_uri=NAMESPACE_URI, namespace_prefix=NAMESPACE_PREFIX)

    results = graph.query(instancesQuery)
    instances = []
    # For each result, create an instance object
    for result in results:
        instance = result[0]
        instanceLabel = result[1]
        color = result[2]
        type = result[3]
        instance = create_resource_obj(instance, instanceLabel, "", color, type, is_dev)
        instances.append(instance)

    return instances


# Get all properties of a resource
def get_properties_of_resource(resource, graph, is_dev):
    # Get all properties of the individual
    propertiesQuery = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    SELECT DISTINCT ?property ?propertyLabel ?propertyValue ?valueLabel ?propertyType ?optionalValueColor
    WHERE {{
        <{individual}> ?property ?propertyValue .
        {{ ?property a rdf:Property }}
        UNION
        {{ ?property a owl:DatatypeProperty }}
        UNION
        {{ ?property a owl:ObjectProperty }}
        ?property a ?propertyType .
        OPTIONAL {{ ?property rdfs:label ?propertyLabel . }}
        OPTIONAL {{ ?propertyValue rdfs:label ?valueLabel . }}
        OPTIONAL {{ ?propertyValue {namespace_prefix}:typicalHsl ?optionalValueColor . }}
    }}
    """.format(individual=resource, namespace_prefix=NAMESPACE_PREFIX)

    results = graph.query(propertiesQuery)
    properties = []
    # For each result, create a property object
    for result in results:
        property = result[0]
        propertyLabel = result[1]
        propertyValue = htmlify(result[2])
        valueLabel = result[3]
        propertyType = result[4]
        valueColor = result[5]
        
        prop = create_resource_obj(property, propertyLabel, "",  "", propertyType, is_dev)
        val = create_resource_obj(propertyValue, valueLabel, "", valueColor, "", is_dev)


        properties.append({"property": prop, "value": val})

    return properties


