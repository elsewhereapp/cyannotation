from ontology import *
from color_list import *
import os, shutil, re
from color_names import *
from color_types import *
from color_values import *
from helpers_url import *
from create_html import *
from namespace_definitions import *

BASE_URL = "https://www.cyannotation.org/"
NAMESPACE_URI = BASE_URL + "ontology/"
cyanNS = Namespace(NAMESPACE_URI)

metallicSheen = color_types["metallic_sheen"]

cyannotationStandardHueSet = cyanNS["CyannotationStandardHueSet"]
cyannotationStandardSaturationSet = cyanNS["CyannotationStandardSaturationSet"]
cyannotationStandardLightnessSet = cyanNS["CyannotationStandardLightnessSet"]
cyannotationStandardMetallicSheenSet = cyanNS["CyannotationStandardMetallicSheenSet"]
cyannotationStandardColorCategorySet = cyanNS["CyannotationStandardColorCategorySet"]
cyannotationStandardOpacitySet = cyanNS["CyannotationStandardOpacitySet"]
cyannotationStandardGlossSet = cyanNS["CyannotationStandardGlossSet"]

# Function to split on capital letters
def split_on_capital_letters(string):
    return re.sub('([A-Z][a-z]+)', r' \1', string).split()

# Function to decapitalise every word in an array of strings
def decapitalise_every_word(list_of_strings):
    return [word.lower() for word in list_of_strings]

# Function to join a list of strings with a space
def join_list_of_strings(list_of_strings):
    return " ".join(list_of_strings)

# Function to turn an id into a label
def id_to_description(id):
    # split on capital letters
    words = split_on_capital_letters(id)
    # decapitalise every word
    words = decapitalise_every_word(words)
    # join with a space
    label = join_list_of_strings(words)

    return label





def create_individuals():

    #########################
    # ATTRIBUTE SETS
    #########################

    attribute_sets = [
        (cyannotationStandardHueSet, rdfType, hueSetClass),
        (cyannotationStandardHueSet, rdfsLabel, Literal("Cyannotation Standard Hue Set", datatype=xsdString)),
        (cyannotationStandardHueSet, rdfsComment, Literal("The built-in hue set of Cyannotation. Feel free to create your own hue set.", datatype=xsdString)),

        (cyannotationStandardSaturationSet, rdfType, saturationSetClass),
        (cyannotationStandardSaturationSet, rdfsLabel, Literal("Cyannotation Standard Saturation Set", datatype=xsdString)),
        (cyannotationStandardSaturationSet, rdfsComment, Literal("The built-in saturation set of Cyannotation. Feel free to create your own saturation set.", datatype=xsdString)),

        (cyannotationStandardLightnessSet, rdfType, lightnessSetClass),
        (cyannotationStandardLightnessSet, rdfsLabel, Literal("Cyannotation Standard Lightness Set", datatype=xsdString)),
        (cyannotationStandardLightnessSet, rdfsComment, Literal("The built-in lightness set of Cyannotation. Feel free to create your own lightness set.", datatype=xsdString)),

        (cyannotationStandardMetallicSheenSet, rdfType, metallicSheenSetClass),
        (cyannotationStandardMetallicSheenSet, rdfsLabel, Literal("Cyannotation Standard Metallic Sheen Set", datatype=xsdString)),
        (cyannotationStandardMetallicSheenSet, rdfsComment, Literal("The built-in metallic sheen set of Cyannotation. Feel free to create your own metallic sheen set.", datatype=xsdString)),

        (cyannotationStandardColorCategorySet, rdfType, colorCategorySetClass),
        (cyannotationStandardColorCategorySet, rdfsLabel, Literal("Cyannotation Standard Color Category Set", datatype=xsdString)),
        (cyannotationStandardColorCategorySet, rdfsComment, Literal("The built-in color category set of Cyannotation. Feel free to create your own color category set.", datatype=xsdString)),

        (cyannotationStandardOpacitySet, rdfType, opacitySetClass),
        (cyannotationStandardOpacitySet, rdfsLabel, Literal("Cyannotation Standard Opacity Set", datatype=xsdString)),
        (cyannotationStandardOpacitySet, rdfsComment, Literal("The built-in opacity set of Cyannotation. Feel free to create your own opacity set.", datatype=xsdString)),

        (cyannotationStandardGlossSet, rdfType, glossSetClass),
        (cyannotationStandardGlossSet, rdfsLabel, Literal("Cyannotation Standard Gloss Set", datatype=xsdString)),
        (cyannotationStandardGlossSet, rdfsComment, Literal("The built-in gloss set of Cyannotation. Feel free to create your own gloss set.", datatype=xsdString)),
    ]


    individuals = []

    # Make simple colors
    for key, value in simpleColorList.items():
        colorId = key[0].upper() + key[1:]
        hue = value["hslType"][0]
        saturation = value["hslType"][1]
        lightness = value["hslType"][2]
        metallic_sheen = metallicSheen["NOT_METALLIC"]
        if "metallic_sheen" in value:
            metallic_sheen = value["metallic_sheen"]
        newColor = cyanNS[colorId]
        displayHslString = 'hsl(' + str(value['hsl'][0]) + ', ' + str(value['hsl'][1]) + '%, ' + str(value['hsl'][2]) + '%)'
        info = value['info']

        label = info['name']
        dbpediaIri = info['dbpedia_iri']
        dbpedia_name = info['dbpediaName']

        # Remove namespace from dbpediaIri
        dbpediaId = dbpediaIri.replace(DBPEDIA_IRI, '')
        thing = dbpediaNS[dbpediaId]

        individuals.append((newColor, rdfType, colorClass))
        if dbpedia_name: 
            individuals.append((thing, rdfsLabel, Literal(dbpedia_name, datatype=xsdString)))
            individuals.append((newColor, owlSameAs, thing))

        individuals.append((newColor, name, Literal(label, datatype=xsdString)))
        individuals.append((newColor, typicalHsl, Literal(displayHslString, datatype=xsdString)))
        individuals.append((newColor, rdfsLabel, Literal(label, datatype=xsdString)))

        # Add color categories
        categories = value['associations']
        for category in categories:
            individuals.append((newColor, hasColorCategory, cyanNS[category]))
            individuals.append((cyanNS[category], colorCategoryOf, newColor))

        # Add which colours it is a type of
        if "variantOf" in value:
            for colorType in value["variantOf"]:
                colorId = colorType["name"].replace(" ", "")
                individuals.append((newColor, variantOf, cyanNS[colorId]))
                individuals.append((cyanNS[colorId], hasVariant, newColor))

        # Add which colours are its variants
        if "hasVariant" in value:
            for colorType in value["hasVariant"]:
                colorId = colorType["name"].replace(" ", "")
                individuals.append((newColor, hasVariant, cyanNS[colorId]))
                individuals.append((cyanNS[colorId], variantOf, newColor))


    # Iterate over both key and value in complexColors
    # And make the complex colors
    for key, value in colorList.items():
        colorId = key[0].upper() + key[1:]
        hue = value["hslType"][0]
        saturation = value["hslType"][1]
        lightness = value["hslType"][2]

        metallic_sheen = metallicSheen["NOT_METALLIC"]
        if "metallic_sheen" in value:
            metallic_sheen = value["metallic_sheen"]


        newColor = cyanNS[colorId]
        displayHslString = 'hsl(' + str(value['hsl'][0]) + ', ' + str(value['hsl'][1]) + '%, ' + str(value['hsl'][2]) + '%)'

        info = value['info']

        label = info['name']
        dbpediaIri = info['dbpedia_iri']
        dbpedia_name = info['dbpediaName']

        # Remove namespace from dbpediaIri
        dbpediaId = dbpediaIri.replace(DBPEDIA_IRI, '')
        thing = dbpediaNS[dbpediaId]

        individuals.append((newColor, rdfType, colorClass))
        if dbpedia_name: 
            individuals.append((thing, rdfsLabel, Literal(dbpedia_name, datatype=xsdString)))
            individuals.append((newColor, owlSameAs, thing))

        # Hue
        individuals.append((newColor, hasHue, cyanNS[hue]))
        individuals.append((cyanNS[hue], hueOf, newColor))

        # Saturation
        individuals.append((newColor, hasSaturation, cyanNS[saturation]))
        individuals.append((cyanNS[saturation], saturationOf, newColor))

        # Lightness
        individuals.append((newColor, hasLightness, cyanNS[lightness]))
        individuals.append((cyanNS[lightness], lightnessOf, newColor))

        # Metallic Sheen
        individuals.append((newColor, hasMetallicSheen, cyanNS[metallic_sheen]))
        individuals.append((cyanNS[metallic_sheen], metallicSheenOf, newColor))

        individuals.append((newColor, name, Literal(label, datatype=xsdString)))
        individuals.append((newColor, typicalHsl, Literal(displayHslString, datatype=xsdString)))
        individuals.append((newColor, rdfsLabel, Literal(label, datatype=xsdString)))

        # Add which colours it is a type of
        if "variantOf" in value:
            for colorType in value["variantOf"]:
                colorId = colorType["name"].replace(" ", "")
                individuals.append((newColor, variantOf, cyanNS[colorId]))
                individuals.append((cyanNS[colorId], hasVariant, newColor))

        # Add color categories
        categories = value['associations']
        for category in categories:
            individuals.append((newColor, hasColorCategory, cyanNS[category]))
            individuals.append((cyanNS[category], colorCategoryOf, newColor))

            # Naively assume that we can slice off the "ColorCategory" part of the category name to make the parent color
            # This is a hack, but it works for now
            parentColorName = category[:-13]
            if not parentColorName == "Metallic":
                individuals.append((newColor, variantOf, cyanNS[parentColorName]))
                individuals.append((cyanNS[parentColorName], hasVariant, newColor))


    # Add hues
    hueNames = color_types["hues"]
    for key, value in hueNames.items():
        hueId = value["id"]
        descriptionText = "The " + id_to_description(hueId) + " in the color spectrum, as opposed to the color."
        if hueId == "NoHue":
            descriptionText = "No perceivable hue. In terms a color space there may be a nominal hue, but it is not perceivable (as a result of too little saturation, to much or little lightness, etc)."
        if hueId == "WarmHue":
            descriptionText = "A warm hue, as opposed to a cool hue."
        if hueId == "CoolHue":
            descriptionText = "A cool hue, as opposed to a warm hue."

        newHue = cyanNS[hueId]
        individuals.append((newHue, rdfType, hueClass))
        individuals.append((newHue, rdfsLabel, Literal(hueId, datatype=xsdString)))
        individuals.append(((newHue, description, Literal(descriptionText, datatype=xsdString))))
        individuals.append((newHue, memberOfSet, cyannotationStandardHueSet))
        individuals.append((cyannotationStandardHueSet, hasSetMember, newHue))

        # Add preferredDescriptors
        if "preferredDescriptor" in value:
            for descriptor in value["preferredDescriptor"]:
                individuals.append((newHue, preferredDescriptor, Literal(descriptor, datatype=xsdString)))

    # Add saturations
    saturationNames = color_types["saturation_name"]
    for key, value in saturationNames.items():
        saturationId = value["id"]
        subAttributes = value["subAttributeOf"]
        newSaturation = cyanNS[saturationId]
        individuals.append((newSaturation, rdfType, saturationClass))
        individuals.append((newSaturation, rdfsLabel, Literal(saturationId, datatype=xsdString)))
        individuals.append(((newSaturation, description, Literal(saturation_descriptions[key], datatype=xsdString))))
        individuals.append((newSaturation, memberOfSet, cyannotationStandardSaturationSet))
        individuals.append((cyannotationStandardSaturationSet, hasSetMember, newSaturation))

        # Add which saturations it is a sub-attribute of
        for subAttr in subAttributes:
            individuals.append((newSaturation, subAttributeOf, cyanNS[subAttr]))
            individuals.append((cyanNS[subAttr], hasSubAttribute, newSaturation))

    # Add lightnesses
    lightnessNames = color_types["lightness"]
    for key, value in lightnessNames.items():
        lightnessId = value["id"]
        newLightness = cyanNS[lightnessId]
        individuals.append((newLightness, rdfType, lightnessClass))
        individuals.append((newLightness, rdfsLabel, Literal(lightnessId, datatype=xsdString)))
        individuals.append(((newLightness, description, Literal(lightness_descriptions[key], datatype=xsdString))))
        individuals.append((newLightness, memberOfSet, cyannotationStandardLightnessSet))
        individuals.append((cyannotationStandardLightnessSet, hasSetMember, newLightness))

        # Add which lightnesses it is a sub-attribute of
        for subAttr in value["subAttributeOf"]:
            individuals.append((newLightness, subAttributeOf, cyanNS[subAttr]))
            individuals.append((cyanNS[subAttr], hasSubAttribute, newLightness))

    # Add metallic sheen
    metallicNames = color_types["metallic_sheen"]
    for key, value in metallicNames.items():
        metallicId = value
        newMetallic = cyanNS[metallicId]
        individuals.append((newMetallic, rdfType, metallicSheenClass))
        individuals.append((newMetallic, rdfsLabel, Literal(value, datatype=xsdString)))
        individuals.append(((newMetallic, description, Literal(metallic_sheen_descriptions[key], datatype=xsdString))))
        individuals.append((newMetallic, memberOfSet, cyannotationStandardMetallicSheenSet))
        individuals.append((cyannotationStandardMetallicSheenSet, hasSetMember, newMetallic))

    # Add opacities
    opacityNames = color_types["opacity"]
    for key, value in opacityNames.items():
        opacityId = value
        newOpacity = cyanNS[opacityId]
        individuals.append((newOpacity, rdfType, opacityClass))
        individuals.append((newOpacity, rdfsLabel, Literal(value, datatype=xsdString)))
        individuals.append(((newOpacity, description, Literal(opacity_descriptions[key], datatype=xsdString))))
        individuals.append((newOpacity, memberOfSet, cyannotationStandardOpacitySet))
        individuals.append((cyannotationStandardOpacitySet, hasSetMember, newOpacity))

    # Add glosses
    glossNames = color_types["gloss"]
    for key, value in glossNames.items():
        glossId = value
        newGloss = cyanNS[glossId]
        individuals.append((newGloss, rdfType, glossClass))
        individuals.append((newGloss, rdfsLabel, Literal(value, datatype=xsdString)))
        individuals.append(((newGloss, description, Literal(gloss_descriptions[key], datatype=xsdString))))
        individuals.append((newGloss, memberOfSet, cyannotationStandardGlossSet))
        individuals.append((cyannotationStandardGlossSet, hasSetMember, newGloss))

    # Add color categories
    colorCategoryNames = color_types["color_category"]
    for key, value in colorCategoryNames.items():
        descriptionText = "The " + id_to_description(value) + ", as opposed to the color."
        if value == "MetallicColorCategory":
            descriptionText = "Not a formal color category, but an often used colloquial one (e.g. when talking about sets of colored pencils)."
        colorCategoryId = value
        newColorCategory = cyanNS[colorCategoryId]
        individuals.append((newColorCategory, rdfType, colorCategoryClass))
        individuals.append((newColorCategory, rdfsLabel, Literal(value, datatype=xsdString)))
        individuals.append(((newColorCategory, description, Literal(descriptionText, datatype=xsdString))))

    # attribute_sets

    for attribute_set in attribute_sets:
        individuals.append(attribute_set)

    PATH_TO_ONTOLOGY = 'public/ontology/'

    # Delete the contents of the 'resources' folder
    # In preparation for adding new resources
    for filename in os.listdir(PATH_TO_ONTOLOGY):
        # Don't try to remove dot files
        if filename.startswith('.'):
            continue
        shutil.rmtree(PATH_TO_ONTOLOGY + filename)

    return individuals





