from colormath.color_objects import sRGBColor, LabColor
from colormath.color_conversions import convert_color 
import numpy as np
from color_list import colorList

# No idea why this isn't online in python
# It's converted from Javascript, https://css-tricks.com/converting-color-spaces-in-javascript/
def hsl_to_rgb(h,s,l):
    # Must be fractions of 1
    s /= 100
    l /= 100

    c = (1 - abs(2 * l - 1)) * s
    x = c * (1 - abs((h / 60) % 2 - 1))
    m = l - c/2
    r = 0
    g = 0
    b = 0

    if 0 <= h and h < 60:
        r = c
        g = x
        b = 0
    elif 60 <= h and h < 120:
        r = x
        g = c
        b = 0
    elif 120 <= h and h < 180:
        r = 0
        g = c
        b = x
    elif 180 <= h and h < 240:
        r = 0
        g = x
        b = c
    elif 240 <= h and h < 300:
        r = x
        g = 0
        b = c
    elif 300 <= h and h < 360:
        r = c
        g = 0
        b = x

    r = round((r + m) * 255)
    g = round((g + m) * 255)
    b = round((b + m) * 255)

    return [r,g,b]

# Convert RGB to CIELAB
def rgb_to_cielab(a):
    """
    a is a pixel with RGB coloring
    """
    a1,a2,a3 = a/255

    color1_rgb = sRGBColor(a1, a2, a3)

    color1_lab = convert_color(color1_rgb, LabColor)

    return color1_lab

# Convert HSL to CIELAB
# This works in a very similar way to http://colormine.org/convert/rgb-to-lab
# But it's different to ADOBE for whatever reason
def hsl_to_cielab(h,s,l):
    """
    h is a hue in degrees
    s is a saturation in percent
    l is a lightness in percent
    """
    r,g,b = hsl_to_rgb(h,s,l)
    lab_color_obj = rgb_to_cielab(np.array([r,g,b]))

    # Get the LAB values 
    lab_l = lab_color_obj.lab_l
    lab_a = lab_color_obj.lab_a
    lab_b = lab_color_obj.lab_b

    return [lab_l, lab_a, lab_b]


# Get the distance between two HSL colors
def get_color_distance(hsl1, hsl2):
    """
    hsl1 is a tuple of (hue, saturation, lightness)
    hsl2 is a tuple of (hue, saturation, lightness)
    """
    lab1 = hsl_to_cielab(hsl1[0], hsl1[1], hsl1[2])
    lab2 = hsl_to_cielab(hsl2[0], hsl2[1], hsl2[2])

    # Use euclidean distance (Square root of sum of squares)
    return np.sqrt((lab1[0] - lab2[0])**2 + (lab1[1] - lab2[1])**2 + (lab1[2] - lab2[2])**2)


# Get the closest color in the color list to a given HSL color
def get_closest_color(hsl):
    """
    hsl is a tuple of (hue, saturation, lightness)
    """
    min_distance = 1000
    closest_color = None

    for key, value in colorList.items():
        distance = get_color_distance(hsl, value['hsl'])
        if distance < min_distance:
            min_distance = distance
            closest_color = value['info']['name']

    return closest_color


